﻿using System;
using System.Diagnostics;
using Acr.Settings;
using com.knetikcloud.Api;
using com.knetikcloud.Client;
using com.knetikcloud.Model;
using Cuentas.Utils;
using HabSDK.Api;
using Prism;
using Prism.Ioc;
using Prism.Navigation;
using Prism.Unity;
using Property.Models;
using Property.Services;
using Property.Services.Abstractions;
using Property.ViewModels;
using Property.Views;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
namespace Property
{
    public partial class App : PrismApplication
    {
        public INavigationService Navigation { get; set; }
        public App(IPlatformInitializer initializer = null) : base(initializer)
        {
            Navigation = NavigationService;
        }
        //TODO: Need to add SettingsPlugin and SecureStorage to update the current user and keep tokens
        public static UserResource CurrentUser { get; set; }
        protected override void OnInitialized()
        {
            try
            {
                InitializeComponent();
                var oauth2 = Settings.Current.Get<OAuthEntity>("OAuthToken");
                var currentUser = Settings.Current.Get<UserResource>("CurrentUser");
                if (oauth2 != null && currentUser != null)
                {
                    Container.Resolve<IAccessTokenApiService>().ValidateTokenAsync();
                    NavigationService.NavigateAsync("Navigation/UnitListPage");
                }
                else
                {
                    NavigationService.NavigateAsync("Navigation/Login");
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
        }

        protected override void OnResume()
        {
            var oauth2 = Settings.Current.Get<OAuthEntity>("OAuthToken");
            var currentUser = Settings.Current.Get<UserResource>("CurrentUser");
            if (oauth2 != null && currentUser != null)
            {
                Container.Resolve<IAccessTokenApiService>().ValidateTokenAsync();
            }
        }

        protected override void RegisterTypes(IContainerRegistry containerRegistry)
        {
            Configuration.Default.BasePath = Constants.KNETIK_JSPAI_URL;
            HabSDK.Client.Configuration.Default.BasePath = Constants.HAB_API_LINK;

            containerRegistry.RegisterForNavigation<NavigationPage>("Navigation");
            containerRegistry.RegisterForNavigation<LoginPage>("Login");
            containerRegistry.RegisterForNavigation<HomePage>("Home");
            containerRegistry.RegisterForNavigation<PropertySetupPage>("PropertySetup");
            containerRegistry.RegisterForNavigation<UnitListPage>("UnitListPage");

            containerRegistry.RegisterInstance(typeof(IAccessTokenApiService), new AccesTokenApiService());
            containerRegistry.RegisterInstance(typeof(IUsersApi), new UsersApi());
            containerRegistry.RegisterInstance(typeof(IUsersGroupsApi), new UsersGroupsApi());
            containerRegistry.RegisterInstance(typeof(IPlacesApi), new PlacesApi());
            containerRegistry.RegisterInstance(typeof(HabSDK.Api.IDevicesApi), new HabSDK.Api.DevicesApi());

        }
    }
}

