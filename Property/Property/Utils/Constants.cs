﻿namespace Cuentas.Utils
{
    public class Constants
    {
        public static string SECRET_KEY = "";
        public static string CLIENT_KEY = "knetik";
        public static string ClIENT_ID = "knetik";

        public static string KNETIK_JSPAI_URL = "https://thermo-dev.devsandbox.knetikcloud.com";
        public static string KNETIK_NOTIFICATION_URL = "https://thermo-dev.devsandbox.knetikcloud.com";

        public static string ZONE_NEW_LINK = "https://g2mz9i0ws7.execute-api.us-east-1.amazonaws.com/dev";
        public static string HAB_API_LINK = "https://hab-api.devsandbox.knetikcloud.com";

        public static string APP_KEY = "x-knetikcloud-appid"; 

        public enum GrandTypes
        {
            client_credentials,
            password,
            facebook,
            google,
            refresh_token
        }

        public enum AppKeys
        {
            Email,
            password,
            appName
        }
    }
}