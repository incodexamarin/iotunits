﻿using System;
using com.knetikcloud.Model;
using Xamarin.Forms;

namespace Property.Utils
{
    public class Utils
    {
        public static void SaveCredentials(string key, string value)
        {
            if (!string.IsNullOrWhiteSpace(key) && !string.IsNullOrWhiteSpace(value))
            {
                if (Application.Current.Properties.ContainsKey(key))
                {
                    Application.Current.Properties[key] = value;
                }
                else
                {

                    Application.Current.Properties.Add(key, value);

                }

            }
            Application.Current.SavePropertiesAsync();
        }

        public static void SaveCurrentUser(string key, object value)
        {
            if (!string.IsNullOrWhiteSpace(key) && value != null)
            {
                if (Application.Current.Properties.ContainsKey(key))
                {
                    Application.Current.Properties[key] = value;
                }
                else
                {

                    Application.Current.Properties.Add(key, value);

                }

            }
            Application.Current.SavePropertiesAsync();
        }

        public static object GetCurrentUser(string key)
        {
            if (!string.IsNullOrWhiteSpace(key))
            {
                if (Application.Current.Properties.ContainsKey(key))
                {
                    return Application.Current.Properties[key];
                }


            }
            return "";
        }


        public static string GetCredentials(string key)
        {
            if (!string.IsNullOrWhiteSpace(key))
            {
                if (Application.Current.Properties.ContainsKey(key))
                {
                    return Application.Current.Properties[key].ToString();
                }


            }
            return "";
        }


        public static string GetCredentials(string key,string defaultV)
        {
            if (!string.IsNullOrWhiteSpace(key))
            {
                if (Application.Current.Properties.ContainsKey(key))
                {
                    return Application.Current.Properties[key].ToString();
                }


            }
            return defaultV;
        }
    }
}
