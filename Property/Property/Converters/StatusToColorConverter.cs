﻿using System;
using System.Globalization;
using com.knetikcloud.Model;
using HabSDK.Model;
using Xamarin.Forms;

namespace Property.Converters
{
    public class StatusToColorConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var color = Color.Transparent;
            if (value != null)
            {
                var state = (PlaceResource.StatusEnum)value;
                switch (state)
                {
                    case PlaceResource.StatusEnum.Active:
                        return Color.LightGreen;
                    case PlaceResource.StatusEnum.Inactive:
                        return Color.LightCoral;
                    default:
                        return Color.White;
                }
            }
            return color;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var state = (PlaceResource.StatusEnum)value;
            switch (state)
            {
                case PlaceResource.StatusEnum.Active:
                    return Color.LightGreen;
                case PlaceResource.StatusEnum.Inactive:
                    return Color.LightCoral;
                default:
                    return Color.White;
            }
        }
    }
}