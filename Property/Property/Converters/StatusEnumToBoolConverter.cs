﻿using System;
using System.Globalization;
using com.knetikcloud.Model;
using HabSDK.Model;
using Xamarin.Forms;

namespace Property.Converters
{
    public class StatusEnumToBoolConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            PlaceResource.StatusEnum status = (PlaceResource.StatusEnum)value;
            return status == PlaceResource.StatusEnum.Active;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            bool statusBool = (bool) value;
            return statusBool ? PlaceResource.StatusEnum.Active : PlaceResource.StatusEnum.Inactive;
        }
    }
}