﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace Property.Models
{
    public class DummyData
    {
        public List<Unit> dummyunits = new List<Unit>()
        {
            new Unit() {name = "unit1", isEnabled = true},
            new Unit() {name = "unit2", isEnabled = false},
            new Unit() {name = "unit3", isEnabled = true},
            new Unit() {name = "unit4", isEnabled = false},
            new Unit() {name = "unit5", isEnabled = true},
            new Unit() {name = "unit6", isEnabled = true},
            new Unit() {name = "unit7", isEnabled = false}
        };

        public List<PropertyObject> dummyProperties = new List<PropertyObject>()
        {
            new PropertyObject() {name = "property1"},
            new PropertyObject() {name = "property2"},
            new PropertyObject() {name = "property3"},
            new PropertyObject() {name = "property4"},
            new PropertyObject() {name = "property5"},
            new PropertyObject() {name = "property6"},
            new PropertyObject() {name = "property7"}
        };

        public List<Building> dummyBuilding = new List<Building>()
        {
            new Building() {name = "Building1"},
            new Building() {name = "Building2"},
            new Building() {name = "Building3"},
            new Building() {name = "Building4"},
            new Building() {name = "Building5"},
            new Building() {name = "Building6"},
            new Building() {name = "Building7"}
        };

    }


    public class Unit
    {


        public String name {set;get; }


        public bool isEnabled { set; get; }

    }

    public class PropertyObject
    {

        public string name { set; get; }

    }

    public class Building
    {

        public string name { set; get; }

    }



}