﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using com.knetikcloud.Model;
using Xamarin.Forms;

namespace Property.Views
{
    public partial class UnitListPage : ContentPage
    {
        public UnitListPage()
        {
            NavigationPage.SetHasBackButton(this, false);
            InitializeComponent();
        }
    }
}
