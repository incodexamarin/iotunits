﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using Property.Converters;
using Property.ViewModels;
using Xamarin.Forms;
using Xamvvm;

namespace Property.Views
{
    public partial class PropertySetupPage : ContentPage
    {
        public PropertySetupPage()
        {
            Resources = new ResourceDictionary
            {{"TagValidatorFactory", new Func<string, object>(
                    (arg) => (BindingContext as PropertySetupPageViewModel)?.ValidateAndReturn(arg))},
                {"StatusConverter", new StatusEnumToBoolConverter()}};
            InitializeComponent();  
            
        }
    }
}
