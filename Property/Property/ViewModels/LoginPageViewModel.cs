﻿using Prism.Commands;
using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Acr.Settings;
using Acr.UserDialogs;
using com.knetikcloud.Api;
using com.knetikcloud.Client;
using com.knetikcloud.Model;
using Cuentas.Utils;
using Prism.Navigation;
using Prism.Services;
using Property.Models;
using Property.Services.Abstractions;
using Xamarin.Forms;

namespace Property.ViewModels
{
    public class LoginPageViewModel : BaseViewModel
    {
        private readonly IPageDialogService _pageDialogService;
        private readonly IUsersApi _usersApi;

        public LoginPageViewModel(INavigationService navigationService, IAccessTokenApiService accessTokenApiService,
            IPageDialogService pageDialogService, IUsersApi usersApi) : base(
            navigationService, accessTokenApiService)
        {
            _usersApi = usersApi;
            _pageDialogService = pageDialogService;
            Email = Utils.Utils.GetCredentials(Constants.AppKeys.Email.ToString());
            Password = Utils.Utils.GetCredentials(Constants.AppKeys.password.ToString());
            AppName = Utils.Utils.GetCredentials(Constants.AppKeys.appName.ToString(), "thermo-dev");

        }

        private DelegateCommand _loginCommand;

        public DelegateCommand LoginCommand => _loginCommand ?? (_loginCommand =
                                                   new DelegateCommand(OnLoginCommandExecuted, LoginCommandCanExecute)
                                                       .ObservesProperty(() => Email)
                                                       .ObservesProperty(() => Password));


        private string _password;
        public string Password
        {
            get { return _password; }
            set { SetProperty(ref _password, value); }
        }

        private string _email;
        public string Email
        {
            get { return _email; }
            set { SetProperty(ref _email, value); }
        }


        private string _appName;
        public string AppName
        {
            get { return _appName; }
            set { SetProperty(ref _appName, value); }
        }

        private async void OnLoginCommandExecuted()
        {
            try
            {
                UserDialogs.Instance.ShowLoading("Loading...");

                OAuth2Resource result = await _accessTokenApiService.GetOAuthTokenAsync(
                    Constants.GrandTypes.password.ToString(), Constants.ClIENT_ID, "", Email, Password, "", "");
                OAuthEntity response = new OAuthEntity(result, DateTime.UtcNow);
                Settings.Current.Set("OAuthToken", response);
                Settings.Current.Set("AppName", AppName); //AppName = thermo-dev

                Configuration.Default.AccessToken = result.AccessToken;
                HabSDK.Client.Configuration.Default.AccessToken = result.AccessToken;
                HabSDK.Client.Configuration.Default.AddDefaultHeader(Constants.APP_KEY, AppName);

                UserResource user = await _usersApi.GetUserAsync("me");
                Settings.Current.Set("CurrentUser", user);
                await _navigationService.NavigateAsync("UnitListPage");
            }
            catch (Exception exception)
            {
                await _pageDialogService.DisplayAlertAsync("Error", exception.Message, "OK");
            }
            finally
            {
                UserDialogs.Instance.HideLoading();
            }
        }

        private bool LoginCommandCanExecute() =>
        !string.IsNullOrWhiteSpace(Email) && !string.IsNullOrWhiteSpace(Password) && !string.IsNullOrWhiteSpace(AppName) && IsNotBusy && Password.Length >= 6;
    }
}

