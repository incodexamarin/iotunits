﻿using Prism.Commands;
using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Linq;
using Prism.Navigation;
using Property.Services.Abstractions;

namespace Property.ViewModels
{
	public class HomePageViewModel : BaseViewModel
	{
        private DelegateCommand _openPropertyCommand;
      
        public DelegateCommand OpenPropertyCommand => _openPropertyCommand ?? (_openPropertyCommand =
                                                                        new DelegateCommand(openPropertySetupPage, shouldInvoke)
     );
        
	    public HomePageViewModel(INavigationService navigationService, IAccessTokenApiService accessTokenApiService) : base(navigationService, accessTokenApiService)
	    {


	    }

        private void openPropertySetupPage(){

            _navigationService.NavigateAsync("PropertySetup");

        }
        private bool shouldInvoke(){
            return true;
        }

	}
}
