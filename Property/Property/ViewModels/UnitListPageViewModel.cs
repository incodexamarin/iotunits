﻿using Prism.Commands;
using System;
using Prism.Navigation;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using com.knetikcloud.Api;
using Cuentas.Utils;
using Acr.UserDialogs;
using com.knetikcloud.Model;
using System.Windows.Input;
using Acr.Settings;
using com.knetikcloud.Client;
using HabSDK.Api;
using HabSDK.Model;
using Newtonsoft.Json.Linq;
using Prism.Services;
using Property.Services.Abstractions;
using Property.Views;
using Xamarin.Forms;
using Xamarin.Forms.Internals;
using UserResource = com.knetikcloud.Model.UserResource;

namespace Property.ViewModels
{
    public class UnitListPageViewModel : BaseViewModel
    {
        private DelegateCommand<PlaceResource> _itemTappedCommand;
        public ICommand ItemTappedCommand => _itemTappedCommand ?? (_itemTappedCommand =
                                                 new DelegateCommand<PlaceResource>(OnItemTappedCommandExecuted));
        //private DelegateCommand _propertyItemChangedCommand;
        //public DelegateCommand PropertyItemChangedCommand => _propertyItemChangedCommand ?? (_propertyItemChangedCommand =
        //                                                         new DelegateCommand(OnPropertyItemChangedCommandExecuted));

        private DelegateCommand _buildingItemChangedCommand;
        public DelegateCommand BuildingItemChangedCommand => _buildingItemChangedCommand ?? (_buildingItemChangedCommand =
                                                                 new DelegateCommand(OnBuildingItemChangedCommandExecuted));

        private DelegateCommand _filterUnitCommand;
        public DelegateCommand FilterUnitCommand => _filterUnitCommand ?? (_filterUnitCommand =
                                                                 new DelegateCommand(OnBuildingItemChangedCommandExecuted));

        //private DelegateCommand _refreshCommand;
        //public DelegateCommand RefreshCommand => _refreshCommand ?? (_refreshCommand =
        //                                             new DelegateCommand(OnRefreshCommandExecuted));

        private DelegateCommand _createUnitCommand;

        public DelegateCommand CreateUnitCommand => _createUnitCommand ?? (_createUnitCommand =
                                                        new DelegateCommand(OnCreateUnitCommandExecuted,
                                                                CreateUnitCommandCanExecute)
                                                            .ObservesProperty(() => BuildingItem));

        private DelegateCommand _logoutCommand;
        public DelegateCommand LogoutCommand => _logoutCommand ?? (_logoutCommand =
                                                                 new DelegateCommand(OnLogoutCommandExecuted));

        private readonly IPlacesApi _placesApi;
        private readonly IPageDialogService _pagedialogService;

        public UnitListPageViewModel(INavigationService navigationService, IAccessTokenApiService accessTokenApiService, IPlacesApi placesApi, IPageDialogService pageDialogService) : base(navigationService, accessTokenApiService)
        {
            _placesApi = placesApi;

            _pagedialogService = pageDialogService;
        }

        //Should update when ApiService is added to ViewModel 
        private async Task CheckAccessToken()
        {
            await ValidationOAuthToken();
            string accessToken = Configuration.Default.AccessToken;
            _placesApi.Configuration.AccessToken = accessToken;
        }

        public override async void OnNavigatingTo(NavigationParameters parameters)
        {
            if (unitsObserval == null)
            {
                IsBusy = true;
                unitsObserval = new ObservableCollection<PlaceResource>();
                //propertyObserval = new ObservableCollection<ZoneBaseResource>();
                buildingObserval = new ObservableCollection<PlacesTreeNodeResource>();
                //await UpdateProperies();
                await SortRootTree();
                IsBusy = false;
            }

        }

        private bool _isRefreshing;
        public bool IsRefreshing
        {
            get { return _isRefreshing; }
            set { SetProperty(ref _isRefreshing, value); }
        }

        private bool _isFilterStatus;
        public bool IsFilterStatus
        {
            get { return _isFilterStatus; }
            set { SetProperty(ref _isFilterStatus, value); }
        }

        private PlacesTreeNodeResource _buildingItem;
        public PlacesTreeNodeResource BuildingItem
        {
            get { return _buildingItem; }
            set { SetProperty(ref _buildingItem, value); }
        }

        private PlacesTreeResource _rootTree;

        public PlacesTreeResource RootTree
        {
            get { return _rootTree; }
            set { SetProperty(ref _rootTree, value); }
        }

        private ObservableCollection<PlaceResource> _unitsObserval;

        public ObservableCollection<PlaceResource> unitsObserval
        {
            get { return _unitsObserval; }
            set { SetProperty(ref _unitsObserval, value); }
        }


        private ObservableCollection<PlacesTreeNodeResource> _propertyObserval;

        public ObservableCollection<PlacesTreeNodeResource> propertyObserval
        {
            get { return _propertyObserval; }
            set { SetProperty(ref _propertyObserval, value); }
        }


        private ObservableCollection<PlacesTreeNodeResource> _buildingObserval;

        public ObservableCollection<PlacesTreeNodeResource> buildingObserval
        {
            get { return _buildingObserval; }
            set { SetProperty(ref _buildingObserval, value); }
        }

        private async Task SortRootTree()
        {
            try
            {
                await CheckAccessToken();

                unitsObserval.Clear();
                buildingObserval.Clear();
                UserResource user = Settings.Current.Get<UserResource>("CurrentUser");
                var rootLocation = user?.AdditionalProperties?["root_location"];
                string rootId = rootLocation?.Property("value")?.Value.ToString();
                RootTree = await _placesApi.GetPlacesTreeAsync(rootId);

                Debug.WriteLine("Root----------------------------");
                Debug.WriteLine(RootTree.ToJson());
                Debug.WriteLine("--------------------------------");

                RootTree.Children.ForEach(buildingObserval.Add);
                if (buildingObserval.Count > 0 && BuildingItem == null)
                    BuildingItem = buildingObserval[0];
               
            }
            catch (Exception ex)
            {
                UserDialogs.Instance.Toast(ex.Message, TimeSpan.FromSeconds(2));
            }
            finally
            {
                IsBusy = false;
            }
        }

        private bool CreateUnitCommandCanExecute() => BuildingItem != null;

        private void OnCreateUnitCommandExecuted()
        {
            if (BuildingItem == null)
            {
                UserDialogs.Instance.ShowError("Building Can't be null");
                return;
            }
            NavigationParameters parameter = new NavigationParameters {{"building", BuildingItem}};
            _navigationService.NavigateAsync("Navigation/PropertySetup", parameter);
        }

        private void FilterUnitList()
        {
            unitsObserval.Clear();
            BuildingItem?.Children.Where(resource => resource.Status == PlaceResource.StatusEnum.Active == IsFilterStatus)
                .ToList().ForEach(unitsObserval.Add);
        }

        private void OnBuildingItemChangedCommandExecuted()
        {
            try
            {
                IsBusy = true;
                FilterUnitList();
            }
            catch (Exception ex)
            {
                UserDialogs.Instance.Toast(ex.Message, TimeSpan.FromSeconds(2));
            }
            finally
            {
                IsBusy = false;
            }
        }

        #region Unused methods

        //private async void OnRefreshCommandExecuted()
        //{
        //    IsRefreshing = true;
        //    //await UpdateProperty();
        //    IsRefreshing = false;
        //}

        //private async Task UpdateUnitList()
        //{
        //    try
        //    {
        //        if (BuildingItem == null) return;
        //        IsBusy = true;
        //        PageResourceZoneBaseResource list =
        //            await _zonesApi.ListZonesAsync(null, BuildingItem.Id.ToString(), "type_ unit");
        //        unitsObserval.Clear();
        //        list.Content.ForEach(unitsObserval.Add);
        //    }
        //    catch (Exception ex)
        //    {
        //        UserDialogs.Instance.Toast(ex.Message, TimeSpan.FromSeconds(2));
        //    }
        //    finally
        //    {
        //        IsBusy = false;
        //    }
        //}

        //private async Task UpdateProperies()
        //{
        //    try
        //    {
        //        UserDialogs.Instance.ShowLoading("Loading...");
        //        PageResourceZoneBaseResource list =
        //            await _zonesApi.ListZonesAsync(null, null, "type_ property", null, 100, 1);
        //        list.Content.ForEach(propertyObserval.Add);
        //        if (propertyObserval.Count > 0 && PropertyItem == null)
        //            PropertyItem = propertyObserval[0];
        //    }
        //    catch (Exception ex)
        //    {
        //        UserDialogs.Instance.ShowError(ex.Message);
        //    }
        //    finally
        //    {
        //        UserDialogs.Instance.HideLoading();
        //    }
        //}

        //private async void OnBuildingItemChangedCommandExecuted()
        //{
        //    await UpdateUnitList();
        //}


        //private async void OnPropertyItemChangedCommandExecuted()
        //{
        //    try
        //    {
        //        if (PropertyItem == null) return;
        //        IsBusy = true;
        //        PageResourceZoneBaseResource list =
        //            await _zonesApi.ListZonesAsync(null, PropertyItem.Id.ToString(), "type_ building");
        //        buildingObserval.Clear();
        //        list.Content.ForEach(buildingObserval.Add);
        //        if (buildingObserval.Count > 0 && BuildingItem == null)
        //            BuildingItem = buildingObserval[0];
        //    }
        //    catch (Exception ex)
        //    {
        //        UserDialogs.Instance.ShowError(ex.Message);
        //    }
        //    finally
        //    {
        //        IsBusy = false;
        //    }
        //}

        #endregion

        private async void OnItemTappedCommandExecuted(PlaceResource unitBase)
        {
            try
            {
                IsBusy = true;
                UserDialogs.Instance.ShowLoading("Loading...");
                //var unit = await _zonesApi.GetZoneAsync(unitBase.Id.ToString());
                //NavigationParameters parameter = new NavigationParameters { { "unit", unit }, { "building", BuildingItem } };
                NavigationParameters parameter = new NavigationParameters { { "unit", unitBase }, { "building", BuildingItem } };
                UserDialogs.Instance.HideLoading();
                await _navigationService.NavigateAsync("PropertySetup", parameter);
            }
            catch (Exception e)
            {
                await _pagedialogService.DisplayAlertAsync("Error", e.Message, "Ok");
            }
            finally
            {
                IsBusy = false;
            }
        }

        private void OnLogoutCommandExecuted()
        {
            if (Settings.Current.Remove("CurrentUser") && Settings.Current.Remove("OAuthToken"))
                Application.Current.MainPage = new NavigationPage(new LoginPage());
        }
    }
}
