﻿using Prism.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using Prism.Navigation;
using Prism.Services;
using com.knetikcloud.Model;
using Acr.UserDialogs;
using Cuentas.Utils;
using com.knetikcloud.Api;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Windows.Input;
using Acr.Settings;
using com.knetikcloud.Client;
using HabSDK.Api;
using HabSDK.Model;
using Newtonsoft.Json.Linq;
using Property.Services.Abstractions;
using Xamarin.Forms;
using Xamvvm;
using DeviceResource = HabSDK.Model.DeviceResource;


namespace Property.ViewModels
{
    public class PropertySetupPageViewModel : BaseViewModel
    {
        private DelegateCommand _qrScanDisplaySerialCommand;
        public DelegateCommand QrsScanDisplaySerialCommand => _qrScanDisplaySerialCommand ?? (_qrScanDisplaySerialCommand =
                                                                   new DelegateCommand(OnScanDisplaySerialCommandExecuted,
                                                                       isScannedEnabled));

        private DelegateCommand _qrScanBaseSerialCommand;
        public DelegateCommand QrsScanBaseSerialCommand => _qrScanBaseSerialCommand ?? (_qrScanBaseSerialCommand =
                                                                new DelegateCommand(OnScanBaseSerialCommandExecuted, isScannedEnabled));

        private DelegateCommand _qrScanGateWaySerialCommand;
        public DelegateCommand QrScanGateWaySerialCommand => _qrScanGateWaySerialCommand ?? (_qrScanGateWaySerialCommand =
                                                                   new DelegateCommand(OnScanGatewaySerialCommandExecuted,
                                                                       isScannedEnabled));

        private DelegateCommand _saveUnitCommand;
        public DelegateCommand SaveUnitCommand => _saveUnitCommand ?? (_saveUnitCommand =
                                                            new DelegateCommand(OnSaveUnitCommandExecuted));

        private DelegateCommand _cancelCommand;
        public DelegateCommand CancelCommand => _cancelCommand ?? (_cancelCommand =
                                                            new DelegateCommand(OnCancelCommandExecuted));


        private DelegateCommand<TagItem> _removeTagCommand;

        public ICommand RemoveTagCommand => _removeTagCommand ?? (_removeTagCommand =
                                                new DelegateCommand<TagItem>(OnRemoveTagCommandExecuted));


        private DelegateCommand _saveGateWaySerialCommand;
        public DelegateCommand SaveGateWaySerialCommand => _saveGateWaySerialCommand ?? (_saveGateWaySerialCommand =
                                                    new DelegateCommand(OnSaveGateWaySerialCommandExecuted, SaveGateWaySerialCommandCanExecute)
                                                                        .ObservesProperty(() => GateWaySerial));
        private DelegateCommand _saveDisplaySerialCommand;

        public DelegateCommand SaveDisplaySerialCommand => _saveDisplaySerialCommand ?? (_saveDisplaySerialCommand =
                                                               new DelegateCommand(OnSaveDisplaySerialCommandExecuted,
                                                                       SaveDisplaySerialCommandCanExecute)
                                                                   .ObservesProperty(() => DisplaySerial)
                                                                   .ObservesProperty(() => BaseSerial));
        private DelegateCommand _saveBaseSerialCommand;
        public DelegateCommand SaveBaseSerialCommand => _saveBaseSerialCommand ?? (_saveBaseSerialCommand =
                                                    new DelegateCommand(OnSaveBaseSerialCommandExecuted, SaveBaseSerialCommandCanExecute)
                                                                        .ObservesProperty(() => BaseSerial));


        private readonly IPlacesApi _placesApi;
        private readonly HabSDK.Api.IDevicesApi _deviceApi;
        private readonly IAccessTokenApiService _accessTokenApiService;
        private readonly IPageDialogService _pagedialogService;

        private readonly string _gateWaySerial = "5149013054585173";
        private readonly string _baseSerial = "5149013188754188";
        private readonly string _displaySerial = "5149013117901446";

        public PropertySetupPageViewModel(INavigationService navigationService, IPageDialogService pageDialogService, IPlacesApi placesApi, HabSDK.Api.IDevicesApi devicesApi,
            IAccessTokenApiService accessTokenApiService) : base(navigationService, accessTokenApiService)
        {
            //placesApi.Configuration.AccessToken = accessTokenApi.Configuration.AccessToken;
            //placesApi.Configuration.AddDefaultHeader(Constants.APP_KEY, Utils.Utils.GetCredentials(Constants.AppKeys.appName.ToString()));
            //_placesApi = placesApi;
            _deviceApi = devicesApi;

            _pagedialogService = pageDialogService;
        }

        //Should update when ApiService is added to ViewModel 
        private async Task CheckAccessToken()
        {
            await ValidationOAuthToken();
            string accessToken = Configuration.Default.AccessToken;
            _deviceApi.Configuration.AccessToken = accessToken;
        }

        private PlaceResource _unitObject;
        public PlaceResource UnitObject
        {
            get { return _unitObject; }
            set { SetProperty(ref _unitObject, value); }
        }

        private string _gwSerial;
        public string GateWaySerial
        {
            get { return _gwSerial; }
            set { SetProperty(ref _gwSerial, value); }
        }
        private string _hvacSystemType;
        public string HvacSystemType
        {
            get { return _hvacSystemType; }
            set { SetProperty(ref _hvacSystemType, value); }
        }

        private string _disSerial;
        public string DisplaySerial
        {
            get { return _disSerial; }
            set { SetProperty(ref _disSerial, value); }
        }

        private string _bSerial;
        public string BaseSerial
        {
            get { return _bSerial; }
            set { SetProperty(ref _bSerial, value); }
        }

        #region Unused properties
        //private ZoneBaseResource _detailsObject;
        //public ZoneBaseResource DetailsObject
        //{
        //    get { return _detailsObject; }
        //    set { SetProperty(ref _detailsObject, value); }
        //}

        //private ZoneBaseResource _buildingObject;

        //public ZoneBaseResource BuildingObject
        //{
        //    get { return _buildingObject; }
        //    set { SetProperty(ref _buildingObject, value); }
        //}

        //private string _displaySerial;
        //public string DisplaySerial
        //{
        //    get
        //    {
        //        _displaySerial = GetAdditionalProperty("serial");
        //        return _displaySerial;
        //    }
        //    set
        //    {
        //        SetProperty(ref _displaySerial, value);
        //        SetAdditionalProperty(_displaySerial, "serial");
        //    }
        //}

        //private string _parentName;
        //public string ParentName
        //{
        //    get
        //    {
        //        _parentName = GetAdditionalProperty("parent_location_name");
        //        return _parentName;
        //    }
        //    set
        //    {
        //        SetProperty(ref _parentName, value);
        //        SetAdditionalProperty(_parentName, "parent_location_name");
        //    }
        //}
        #endregion

        #region Unused methods
        //private string GetAdditionalProperty(string propertyName)
        //{
        //    string propertyValue = default(string);
        //    try
        //    {
        //        if (UnitObject?.AdditionalProperties != null &&
        //            UnitObject.AdditionalProperties.ContainsKey(propertyName))
        //        {
        //            com.knetikcloud.Model.Property property = UnitObject.AdditionalProperties[propertyName];
        //            if (property?.Value != null)
        //            {
        //                propertyValue = property.Value;
        //            }
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        UserDialogs.Instance.Toast(e.Message, TimeSpan.FromSeconds(2));
        //    }
        //    return propertyValue;
        //}

        //private void SetAdditionalProperty(params string[] parameters)
        //{
        //    try
        //    {
        //        if (UnitObject.AdditionalProperties == null)
        //            UnitObject.AdditionalProperties = new Dictionary<string, com.knetikcloud.Model.Property>();

        //        if (!UnitObject.AdditionalProperties.ContainsKey(parameters[1]) && parameters[0] != null)
        //            UnitObject.AdditionalProperties.Add(parameters[1], new com.knetikcloud.Model.Property(parameters[0]));

        //        if (parameters[0] == null) return;
        //        var property = new com.knetikcloud.Model.Property(parameters[0])
        //        {
        //            Type = "text",
        //            Value = parameters[0]
        //        };
        //        UnitObject.AdditionalProperties[parameters[1]] = property;
        //    }
        //    catch (Exception e)
        //    {
        //        UserDialogs.Instance.Toast(e.Message, TimeSpan.FromSeconds(2));
        //    }
        //}
        #endregion

        public override void OnNavigatedTo(NavigationParameters parameters)
        {
            Items = new ObservableCollection<TagItem>();
            UnitObject = parameters.GetValue<PlaceResource>("unit");
            DeviceObserval = new ObservableCollection<DeviceResource>();
            Debug.WriteLine("Unit----------------------------");
            Debug.WriteLine(JObject.FromObject(UnitObject));
            Debug.WriteLine("--------------------------------");
        }

        private void OnRemoveTagCommandExecuted(TagItem tag)
        {
            if (tag != null) Items?.Remove(tag);
        }

        private async void OnSaveUnitCommandExecuted()
        {
            try
            {
                //UserDialogs.Instance.ShowLoading("Loading...");
                //if (UnitObject.Id != null)
                //    await _zonesApi.UpdateZoneAsync(UnitObject.Id.ToString(), UnitObject);
                //else
                //    await _zonesApi.CreateZoneAsync(UnitObject);
                //UserDialogs.Instance.HideLoading();
                //await _navigationService.GoBackAsync();
            }
            catch (Exception e)
            {
                await _pagedialogService.DisplayAlertAsync("Error", e.Message, "Ok");
            }
        }
        

        private async void OnCancelCommandExecuted()
        {
            await _navigationService.GoBackAsync();
        }



        private async Task<String> scanQrCodeAsync()
        {

#if __ANDROID__
           // Initialize the scanner first so it can track the current context
             MobileBarcodeScanner.Initialize (Application);
#endif

            var scanner = new ZXing.Mobile.MobileBarcodeScanner();


            var propertyCode = await scanner.Scan();

            if (propertyCode == null)
            {

                await _pagedialogService.DisplayAlertAsync("Error", "I am not able to scan your code. please try to write it.", "OK");

                return "";
            }

            return propertyCode.Text;

        }

        public async void OnSaveGateWaySerialCommandExecuted()
        {
            try
            {
                UserDialogs.Instance.ShowLoading("Loading...");
                await CheckAccessToken();

                var result = await _deviceApi.CreateDeviceAsync(new DeviceResource
                {
                    Name = $"{UnitObject.Name} Gateway",
                    Serial = _gateWaySerial,
                    DeviceType = DeviceResource.DeviceTypeEnum.Gateway,
                    Make = "momit",
                    Model = "gateway",
                    TypeId = 1,
                    SystemTypeId = 454,
                    LocationId = UnitObject.Id.ToString(),
                    LocationName = UnitObject.Name,
                    ParentLocationName = UnitObject.ParentLocationName,
                    ParentLocationId = UnitObject.Parent.ToString()
                });
                Debug.WriteLine("Gateway Device------------------");
                Debug.WriteLine(JObject.FromObject(result));
                Debug.WriteLine("--------------------------------");

            }
            catch (Exception e)
            {
                await _pagedialogService.DisplayAlertAsync("Error", e.Message, "Ok");
            }
            finally
            {
                UserDialogs.Instance.HideLoading();
            }
        }

        public async void OnSaveDisplaySerialCommandExecuted()
        {
            try
            {
                UserDialogs.Instance.ShowLoading("Loading...");
                await CheckAccessToken();

                var result = await _deviceApi.CreateDeviceAsync(new DeviceResource
                {
                    Name = $"{UnitObject.Name} Display",
                    Description = $"{UnitObject.Description} Display",
                    LocationId = UnitObject.Id.ToString(),
                    LocationName = UnitObject.Name,
                    Serial = _displaySerial,
                    DeviceType = DeviceResource.DeviceTypeEnum.Thermostat,
                    Make = "momit",
                    Model = "bevel_display",
                    SystemTypeId = Convert.ToDecimal(HvacSystemType),
                    ChildSerial = _baseSerial,
                    TypeId = 11
                });
                Debug.WriteLine("DisplaySerial Device------------ ");
                Debug.WriteLine(JObject.FromObject(result));
                Debug.WriteLine("--------------------------------");

            }
            catch (Exception e)
            {
                await _pagedialogService.DisplayAlertAsync("Error", e.Message, "Ok");
            }
            finally
            {
                UserDialogs.Instance.HideLoading();
            }
        }

        public async void OnSaveBaseSerialCommandExecuted()
        {
            try
            {
                UserDialogs.Instance.ShowLoading("Loading...");
                await CheckAccessToken();

                var result = await _deviceApi.CreateDeviceAsync(new DeviceResource
                {
                    Name = $"{UnitObject.Name} Base",
                    Description = $"{UnitObject.Description} Base",
                    Serial = _baseSerial,
                    DeviceType = DeviceResource.DeviceTypeEnum.Thermostatbase,
                    Make = "momit",
                    Model = "bevel_base",
                    TypeId = 12,
                    LocationId = UnitObject.Id.ToString(),
                    LocationName = UnitObject.Name,
                    SystemTypeId = 458
                });
                Debug.WriteLine("Base Device----------------------");
                Debug.WriteLine(JObject.FromObject(result));
                Debug.WriteLine("--------------------------------");

            }
            catch (Exception e)
            {
                await _pagedialogService.DisplayAlertAsync("Error", e.Message, "Ok");
            }
            finally
            {
                UserDialogs.Instance.HideLoading();
            }
        }

        private bool SaveGateWaySerialCommandCanExecute() =>
            !string.IsNullOrWhiteSpace(GateWaySerial);

        private bool SaveDisplaySerialCommandCanExecute() =>
            !string.IsNullOrWhiteSpace(DisplaySerial) && !string.IsNullOrWhiteSpace(BaseSerial);

        private bool SaveBaseSerialCommandCanExecute() =>
            !string.IsNullOrWhiteSpace(BaseSerial);

        private async void OnScanDisplaySerialCommandExecuted()
        {
            //DisplaySerial = await scanQrCodeAsync();
            DisplaySerial = _displaySerial;
        }

        private async void OnScanBaseSerialCommandExecuted()
        {
            //BaseSerial = await scanQrCodeAsync();
            BaseSerial = _baseSerial;
        }

        private async void OnScanGatewaySerialCommandExecuted()
        {
            //GateWaySerial = await scanQrCodeAsync();
            GateWaySerial = _gateWaySerial;
        }

        private bool isScannedEnabled()
        {
            return IsNotBusy;
        }
      
        public TagItem ValidateAndReturn(string tag)
        {
            if (string.IsNullOrWhiteSpace(tag))
                return null;

            var tagString = tag.StartsWith("#") ? tag : "#" + tag;
           
            if (Items.Any(v => v.Name.Equals(tagString, StringComparison.OrdinalIgnoreCase)))
                return null;

            var itemTag = new TagItem()
            {
                Name = tagString.ToLower()
            };
            return itemTag;
        }

    

        private ObservableCollection<TagItem> _items;
        public ObservableCollection<TagItem> Items
        {
            get { return _items; }
            set { SetProperty(ref _items, value); }
        }

        public class TagItem : BaseModel
        {
            string name;
            public string Name
            {
                get  { return name; }
                set { SetField(ref name, value); }
            }
        }

        private ObservableCollection<DeviceResource> _deviceObserval;

        public ObservableCollection<DeviceResource> DeviceObserval
        {
            get { return _deviceObserval; }
            set { SetProperty(ref _deviceObserval, value); }
        }

    }
}