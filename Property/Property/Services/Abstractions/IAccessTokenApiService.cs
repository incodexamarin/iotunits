﻿using System.Threading.Tasks;
using com.knetikcloud.Api;

namespace Property.Services.Abstractions
{
    public interface IAccessTokenApiService : IAccessTokenApi
    {
        Task ValidateTokenAsync();
    }
}